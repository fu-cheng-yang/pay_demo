package com.yang.demo.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Classname AlipayBean
 * @Description
 * @Date 2022/1/11 10:31
 * @Created by spike
 * @Version: 1.0
 */
/*支付实体对象*/
@Data
@Accessors(chain = true)
public class AlipayBean {
    /*商户订单号，必填*/
    private String outTradeNo;
    /*订单名称，必填*/
    private String subject;
    /*付款金额，必填*/
    private StringBuffer totalAmount;
    /*商品描述，可空*/
    private String body;
    /*超时时间参数*/
    private String timeoutExpress = "10m";
    /*产品码*/
    private String productCode = "FAST_INSTANT_TRADE_PAY";
}
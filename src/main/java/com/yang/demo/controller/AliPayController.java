package com.yang.demo.controller;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.yang.demo.entity.AlipayBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Classname AliPayController
 * @Description
 * @Date 2022/1/11 9:56
 * @Created by spike
 * @Version: 1.0
 */
@Controller
@RequestMapping("/pay")
public class AliPayController {

    private static final Logger log = LoggerFactory.getLogger(AliPayController.class);
    @Value("${notify_url}")
    private String notifyUrl;
    @Value("${return_url}")
    private String returnurl;
    @Value("${public_key}")
    private String publickey;
    @Value("${private_key}")
    private String privateKey;

    @Resource
    private AlipayClient alipayClient;


    @PostMapping(value = "/goAlipay")
    @ResponseBody
    public void goAlipay(AlipayBean alipayBean, HttpServletResponse response) throws AlipayApiException, IOException {
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        //支付成功之后跳转的页面
        request.setReturnUrl(returnurl);
        //支付成功之后 回调的接口(公网地址)
        request.setNotifyUrl("http://101.34.29.201:8011/pay/callback");
        JSONObject bizContent = new JSONObject();
        bizContent.put("out_trade_no", alipayBean.getOutTradeNo());
        bizContent.put("total_amount", alipayBean.getTotalAmount());
        bizContent.put("subject", alipayBean.getSubject());
        bizContent.put("product_code", alipayBean.getProductCode());
        bizContent.put("body", alipayBean.getBody());
        request.setBizContent(bizContent.toString());
        request.setNeedEncrypt(true);
        AlipayTradePagePayResponse aliPayResponse = alipayClient.pageExecute(request);
        if (aliPayResponse.isSuccess()) {
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
        String form = aliPayResponse.getBody();
        response.setContentType("text/html;charset=UTF-8");
        response.getWriter().write(form);//直接将完整的表单html输出到页面
        response.getWriter().flush();
        response.getWriter().close();


    }


    @PostMapping(value = "/callback")
    @ResponseBody
    public boolean aliPayCallBack(HttpServletRequest request) throws UnsupportedEncodingException, AlipayApiException {
        log.info("阿里支付回调成功!!");
        //获取支付宝POST过来反馈信息
        Map<String, String> params = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        parameterMap.forEach((key, value) -> params.put(key, value[0]));
        boolean signVerified = AlipaySignature.rsaCheckV1(params, publickey, "UTF-8", "RSA2"); //调用SDK验证签名
        if (signVerified) {//验证成功
            //商户订单号
            String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"), "UTF-8");
            //支付宝交易号
            String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"), "UTF-8");
            //交易状态
            String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"), "UTF-8");

            if (trade_status.equals("TRADE_FINISHED")) {
                log.info("支付已完结");
                return true;
            } else if (trade_status.equals("TRADE_SUCCESS")) {
                log.info("支付成功");
                return true;
            }

        } else {//验证失败
            System.out.println("支付失败");
        }

        return false;
    }

}

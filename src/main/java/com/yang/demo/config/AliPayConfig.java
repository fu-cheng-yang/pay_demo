package com.yang.demo.config;


import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.charset.StandardCharsets;

/**
 * @Classname AliPayConfig
 * @Description
 * @Date 2022/1/11 15:43
 * @Created by spike
 * @Version: 1.0
 */
@Configuration
public class AliPayConfig {

    @Value("${appid}")
    private String appId;
    @Value("${pid}")
    private String pid;
    @Value("${private_key}")
    private String privateKey;
    @Value("${public_key}")
    private String publickey;
    @Value("${sign_type}")
    private String signType;
    @Value("${open_api_domain}")
    private String openApiDomain;
    @Value("${format}")
    private String format;
     @Value("${encryptKey}")
    private String encryptKey;



    /**
     * @Description 获取支付客户端
     * @Date 15:56 2022/1/11
     * @Param []
     * @return com.alipay.api.AlipayClient
     **/
    @Bean
    public AlipayClient AliPayClient() {
        AlipayClient alipayClient = new DefaultAlipayClient(
                openApiDomain,
                appId,
                privateKey,
                format,
                StandardCharsets.UTF_8.name(),
                publickey,
                signType,
                encryptKey,
                "RSA2");
        return alipayClient;
    }

}
